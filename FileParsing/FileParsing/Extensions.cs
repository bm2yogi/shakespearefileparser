﻿using System.Linq;

namespace FileParsing
{
    public static class Extensions
    {
         public static string ToTitleCase(this string sentence)
         {
             var exceptions = new [] {"a", "an", "the", "of", "for", "in"};

             var chars = sentence.ToLower().ToCharArray();
             var i = 0;

             while(i < chars.Length)
             {
                 if (i == 0 || chars[i-1].Equals(' '))
                 {
                     chars[i] = char.ToUpper(chars[i]);
                 }
                 i++;
             }

             return new string(chars);
         }
    }
}