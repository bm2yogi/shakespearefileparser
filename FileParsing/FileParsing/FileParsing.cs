﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Raven.Abstractions.Data;
using Raven.Client.Document;

namespace FileParsing
{
    [TestFixture]
    public class FileParsing
    {
        const string StartPattern = @"^\s*1\d{3}.*$";
        const string EndPattern = @"^.*THE END.*$";
        const string StartCommentPattern = @"^<<.*$";
        const string EndCommentPattern = @"^.*>>$";

        readonly Func<string, bool> _isStart = line => Regex.IsMatch(line, StartPattern);
        readonly Func<string, bool> _isEnd = line => Regex.IsMatch(line, EndPattern);
        readonly Func<string, bool> _isCommentStart = line => Regex.IsMatch(line, StartCommentPattern);
        readonly Func<string, bool> _isCommentEnd = line => Regex.IsMatch(line, EndCommentPattern);

        [Test]
        public void TestMethodName()
        {
            var rawText = System.IO.File.ReadAllLines(@".\shaks12.txt");

            var index = 0;
            var workObjects = new List<Work>();

            while (index < rawText.Length)
            {
                var stringBuilder = new StringBuilder();
                var workObject = new Work();

                if (_isStart(rawText[index]))
                {
                    stringBuilder.Clear();

                    index = SkipOverBlankLines(rawText, index);
                    workObject.Title = rawText[index].Trim().ToTitleCase();

                    index = SkipOverBlankLines(rawText, index);
                    workObject.Author = rawText[index].Trim();

                    index = SkipOverBlankLines(rawText, index);

                    while (!_isEnd(rawText[index]))
                    {
                        index = SkipOverComments(rawText, index);
                        stringBuilder.AppendLine(rawText[index++]);
                    }

                    workObject.Text = stringBuilder.ToString();
                    workObjects.Add(workObject);
                }

                index++;
            }

            //OutputToConsole(workObjects);
            WriteToRavenDB(workObjects);
        }

        private int SkipOverComments(string[] rawText, int index)
        {
            if (_isCommentStart(rawText[index]))
            {
                while (!_isCommentEnd(rawText[index++])){}
            }

            return index;
        }

        private int SkipOverBlankLines(string[] rawText, int index)
        {
            while (String.IsNullOrWhiteSpace(rawText[++index])) { }
            return index;
        }

        private void OutputToConsole(IEnumerable<Work> workObjects)
        {
            workObjects
                //.Skip(1)
                //.Take(1)
                .ToList()
                .ForEach(
                    t =>
                    Console.WriteLine("Title: {0}{1}Author: {2}{1} Text: {3}{1}", t.Title, Environment.NewLine, t.Author, t.Text));
        }

        public void WriteToRavenDB(IEnumerable<Work> workObjects)
        {
            var parser = ConnectionStringParser<RavenConnectionStringOptions>
                .FromConnectionStringName("RavenDBConnection");

            parser.Parse();

            var store =
                new DocumentStore
                    {ApiKey = parser.ConnectionStringOptions.ApiKey, Url = parser.ConnectionStringOptions.Url}
                    .Initialize();

            using (var session = store.OpenSession())
            {
                workObjects.ToList().ForEach(session.Store);
                session.SaveChanges();
            }
        }
    }
}